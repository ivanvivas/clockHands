package clockHands;

public class Main {

	public static void main(String[] args) {
		Double angle;
		for (String arg : args) {
			/* Split hour and minute values */
			String[] parts = arg.split(":");
			int hours = Integer.parseInt(parts[0].trim());
			int minutes = Integer.parseInt(parts[1].trim());
			
			if ((hours != 0) || (minutes != 0)) {
				angle = handsAngle(hours, minutes);
				System.out.println(angle);
			}
		}
	}
	/* The output displays the smallest positive angle in degrees between the hands for each time */
	private static double handsAngle(int hours, int minutes) {
		double returnAngle = 0.0;
		final double MINUTE_MIN_ANGLE_MOV = 6.0; // 360 / 60
		final double HOUR_MIN_ANGLE_MOV = 5.0 / 60.0;
		
		/* Hands positions */
		double hourPos;
		double minutePos;
		if (hours == 12) {
			hourPos = 0.0;
		}
		if (minutes == 0) {
			if (hours == 12) {
				hourPos = 0.0;
			}
			else {
				hourPos = hours * 5.0;
			}
		}
		else {
			hourPos = (hours * 5.0) + ((double) minutes * HOUR_MIN_ANGLE_MOV);
		}
		
		if (minutes == 0) {
			minutePos = 60.0;
		}
		minutePos = (double) minutes;
		
		/* Minutes difference */
		double minutesDiff = Math.abs(hourPos - minutePos);

		/* Hands angle */
		returnAngle =  minutesDiff * MINUTE_MIN_ANGLE_MOV;
		
		if (returnAngle >= 180) {
			returnAngle = 360 - returnAngle;
		}
		
		if (returnAngle == 360) {
			returnAngle = 0.0;
		}
		
		return returnAngle;
	}

}
